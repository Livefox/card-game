/*







Written By:
Evan Svendsen 001186167
Name		student #

*/




#include<iostream>

#include "./Cards Against Humanity/CAH.h"
#include "./Crazy Eights/CrazyEights.h"
#include "./Go Fish/GoFish.h"
#include "./Old Maid/OldMaid.h"
#include "./7's/Sevens.h"

using namespace std;

int main()
{
	cout<<"1. Crazy Eights"<<endl
		<<"2. Go Fish"<<endl
		<<"3. Cards Against Humanity"<<endl
		<<"4. Old Maid"<<endl
		<<"5. 7's"<<endl<<endl
		<<">> ";

	int game;
	cin >> game;

	switch (game) {
	case 1:
		CrazyEights();
	case 2:
		GoFish();
	case 3:
		CAH();
	case 4:
		OldMaid();
	case 5:
		Sevens();
	}
	
	return 0;
}