﻿/*

 ██████╗ █████╗ ██████╗ ██████╗ ███████╗     █████╗  ██████╗  █████╗ ██╗███╗   ██╗███████╗████████╗    ██╗  ██╗██╗   ██╗███╗   ███╗ █████╗ ███╗   ██╗██╗████████╗██╗   ██╗
██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔════╝    ██╔══██╗██╔════╝ ██╔══██╗██║████╗  ██║██╔════╝╚══██╔══╝    ██║  ██║██║   ██║████╗ ████║██╔══██╗████╗  ██║██║╚══██╔══╝╚██╗ ██╔╝
██║     ███████║██████╔╝██║  ██║███████╗    ███████║██║  ███╗███████║██║██╔██╗ ██║███████╗   ██║       ███████║██║   ██║██╔████╔██║███████║██╔██╗ ██║██║   ██║    ╚████╔╝
██║     ██╔══██║██╔══██╗██║  ██║╚════██║    ██╔══██║██║   ██║██╔══██║██║██║╚██╗██║╚════██║   ██║       ██╔══██║██║   ██║██║╚██╔╝██║██╔══██║██║╚██╗██║██║   ██║     ╚██╔╝
╚██████╗██║  ██║██║  ██║██████╔╝███████║    ██║  ██║╚██████╔╝██║  ██║██║██║ ╚████║███████║   ██║       ██║  ██║╚██████╔╝██║ ╚═╝ ██║██║  ██║██║ ╚████║██║   ██║      ██║
 ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝    ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝       ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝      ╚═╝

*/

#include <iostream>
#include <vector>

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>


#include "CAH.h"
#include "../include/Deck.h"
#include "../include/Player.h"
#include "../include/Cards.h"

using namespace std;

void Setup(vector<Player>&, Deck&, Deck&);
bool Run(vector<Player>);
bool Win(vector<Player>);
void GameOver();
//Maximum Players: 8
int numPlayers;

//Written By: Evan
CAH::CAH() {
	Player().numPlayers(8);
	//Declare the game variables
	vector<Player> Group;
	Deck BlackDeck;
	Deck WhiteDeck;

	//Call the setup parameters (Players, Black Deck, White Deck)
	Setup(Group, BlackDeck, WhiteDeck);
	//Run the game
	while(Run(Group)){
		Group.clear();
		numPlayers = Player().numPlayers(8);
		Setup(Group, BlackDeck, WhiteDeck);
	}
}

//Written By: Evan
void Setup(vector<Player>& Group, Deck& B, Deck& W)
{
	//Initialize the contents of each card type (Black/White)

	vector<string> BlackFace = Cards().blackDeckFace();
	vector<string> WhiteFace = Cards().whiteDeckFace();

	Cards Black[blackSize];
	for (int i = 0; i < blackSize; i++)
		Black[i].setFace(BlackFace[i]);

	Cards White[whiteSize];
	for (int j = 0; j < whiteSize; j++)
		White[j].setFace(WhiteFace[j]);

	//Form the deck of cards from the available card types
	Deck BlackDeck(Black);
	Deck WhiteDeck(White);
	B = BlackDeck;
	W = WhiteDeck;

	Player* temp;
	//Create all remaining players
	for (int k = 0; k < numPlayers; k++)
	{
		temp = new Player(WhiteDeck, 10, k + 1, 0);
		Group.push_back(*temp);
		delete temp;
	}
	temp = NULL;
}

//Written By: Evan
bool Run(vector<Player> Group)
{
	if (Win(Group))
		GameOver();


	for(int i=0; i<Group.size(); i++)
		for(int j=0; j<10; j++)
			cout<<i+1<<": "<<Group.at(i).HAND.at(j)<<endl;


	//Ask to repeat the game
	char input;
	cout << "Play again (Y/N): ";
	cin >> input;
	if (input == 'Y' || input == 'y')
		return true;
	return false;
}

//Written By: Evan
bool Win(vector<Player> Group)
{
	for (int i=0; i<numPlayers-1; i++)
		if (0==1)//.at(i).getScore() == -numPlayers + 14)
			return true;
	return false;
}

//Written By: Evan
void GameOver()
{

}
