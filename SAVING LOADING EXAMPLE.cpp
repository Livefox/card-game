#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <algorithm>

using namespace std;

int main()
{
	//*****************SAVING*******************

	//Theoretical values we wish to save
	int score = 1000;
	int maxcards = 5;
	int numplayer = 8;

	//string that will be saved to the .crd file
	string encode;

	//Sets the text that will be outputted to the save file
	encode.append("[int] ;");
	
	//Each variable in the int category will be stored like this.
	encode += "Score:" + to_string(score) + ";";
	encode += "Max Cards:" + to_string(maxcards) + ";";
	encode += "Num Players:" + to_string(numplayer) + ";";
	
	encode.append("[string] ;");
	
	encode += "All string values will be stored under here.";

	//Replaces all semicolons with a newline character for readability in the file
	replace(encode.begin(), encode.end(), ';', '\n');


	ofstream fout;
	ifstream fin;

	//saves the unencoded version of the variables to text.txt
	//For readability and testing purpopses only
	fout.open("text.txt");
	fout << encode;
	fout.close();

	//Opens the "real" save file
	fout.open("save.crd");

	//encodes the data before saving
	for (int i = 0; i < encode.length(); i++) {
		srand(i);
		encode[i] = encode[i] + floor((sqrt(i + rand() % 100) + 1)); 
	}

	//save
	fout << encode;
	fout.close();


	//****************LOADING*****************

	//Re-open file for saving
	fin.open("save.crd");
	
	string copy;
	fin >> copy;

	//Decrypt the coded text and save to copy
	for (int i = 0; i < copy.length(); i++) {
		srand(i);
		copy[i] = copy[i] - floor((sqrt(i + rand() % 100) + 1));
	}

	cout << copy;
	fin.close();

	cout << endl<<"Test"<<endl;
	
	//create 3 new variables to store the loaded values into
	int var1 = 0;
	int var2 = 1;
	int var3 = -2222;

	//for searching
	int start;
	int end;
	
	//set score
	start = copy.find("Score:") + 6;
	end = copy.find("\n",start);
	var1 = stoi(copy.substr(start, end));
	
	//set max cards
	start = copy.find("Max Cards:") + 10;
	end = copy.find("\n", start);
	var2 = stoi(copy.substr(start, end));
	
	//set num players
	start = copy.find("Num Players:") + 12;
	end = copy.find("\n", start);
	var3 = stoi(copy.substr(start, end));

	//stoi will convert a string value back into an integer

	cout << var1 << endl
		<< var2 << endl
		<< var3;

	cin.get();

	return 0;
}