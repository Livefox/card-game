#include "Hand.h"
#include "Deck.h"

//Constructor
Hand::Hand()
{
}

Hand::~Hand()
{
}

//Written By: Evan
void Hand::Draw(Deck& DrawDeck) 
{
	//If your hand is full of cards
	if (size() >= maxCards)
		return;
	//Reshuffle the deck if empty
	if (DrawDeck.size() == 0)
		DrawDeck.ReShuffle();
	Cards_IN_HAND.push_back(DrawDeck.Remove());
}

//Setters and getters
std::string Hand::at(int o) {return Cards_IN_HAND[o].Face();}
void Hand::SETmaxCards(int num) { maxCards = num; }
int Hand::size() { return Cards_IN_HAND.size(); }
