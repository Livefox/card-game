#include <string>
#include <vector>
#include "Cards.h"

//Constructor
Cards::Cards()
{
}

Cards::~Cards()
{
}

//TO DO: Input all the 52 standard cards
//Written By: Evan
std::vector<std::string> Cards::defaultDeckFace()
{
	std::vector<std::string> def;
	//Semicolon signifies end of deck
	std::string text[defaultSize] = { "Ace of Spades","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p"
										,"q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L"
										,"M","N","O","P","Q","S","T",";" };
	for (int i = 0; i < defaultSize; i++)
		def.push_back(text[i]);
	return def;
}

//TO DO: Input all the black deck cards
//Written By: Evan
std::vector<std::string> Cards::blackDeckFace()
{
	std::vector<std::string> def;
	//Semicolon signifies end of deck
	std::string text[blackSize] = { "Ace of Spades","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p"
									,"q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L"
									,"M","N","O","P","Q","S","T",";" };
	for (int i = 0; i < blackSize; i++)
		def.push_back(text[i]);
	return def;
}

//TO DO: Input all the white deck cards
//Written By: Evan
std::vector<std::string> Cards::whiteDeckFace()
{
	std::vector<std::string> def;
	//Semicolon signifies end of deck
	std::string text[whiteSize] = { "Ace of Spades","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p"
									,"q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L"
									,"M","N","O","P","Q","S","T",";" };
	for (int i = 0; i < whiteSize; i++)
		def.push_back(text[i]);
	return def;
}

void Cards::setFace(std::string face)
{
	message = face;
}

//Setters and getters
std::string Cards::Face() { return message; }

//Written By: Evan
Cards& Cards::operator=(Cards RIGHT)
{
	this->message = RIGHT.Face();
	return *this;
}

//Written By: Evan
bool Cards::operator!=(std::string message)
{
	return this->message == message;
}
