#ifndef DECK_H
#define DECK_H

#include <vector>
#include "Cards.h"

class Deck
{
private:
	std::vector <Cards> Cards_IN_DECK;
	std::vector <Cards> Cards_IN_DISCARD;
	int totalCards = 0;
public:
	Deck();
	Deck(Cards[]);
	virtual ~Deck();
	int size() const;
	void ReShuffle();
	Cards Remove();
};

#endif