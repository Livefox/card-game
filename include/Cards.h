#ifndef CARDS_H
#define CARDS_H
const int blackSize = 47;
const int whiteSize = 47;
const int defaultSize = 47;

#include <string>
#include <vector>

class Cards
{
private:
	std::string message;
public:
	Cards();
	~Cards();
	void setFace(std::string);
	std::vector<std::string> defaultDeckFace();
	std::vector<std::string> blackDeckFace();
	std::vector<std::string> whiteDeckFace();
	std::string Face();
	Cards& operator=(Cards);
	bool operator!=(std::string);	
};
#endif