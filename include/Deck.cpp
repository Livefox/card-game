#include <vector>
#include <iterator>
#include <cstdlib>
#include <ctime> 
#include <stdio.h>
#include "Deck.h"
#include "Cards.h"

//Default Constructor
Deck::Deck(){}

//Constructor : An Array of Cards
//Written By: Evan
Deck::Deck(Cards CardType[])
{
	//Breaks at end of line character ;
	for (int i = 0; CardType[i].Face()!= ";"; i++)
	{
		Cards_IN_DECK.push_back(CardType[i]);
	}
	totalCards = size();
}

Deck::~Deck()
{
}

//Reshuffle the deck
//Written By: Evan
void Deck::ReShuffle()
{
	for (int i = totalCards-1; i > -1; i--)
	{
		Cards_IN_DECK.push_back(Cards_IN_DISCARD[i]);
		Cards_IN_DISCARD.pop_back();
	}
}

//Remove cards from the deck
//Written By: Evan
Cards Deck::Remove()
{
	srand(time(NULL));
	int pickFrom = rand()%size();
	Cards temp = Cards_IN_DECK[pickFrom];
	Cards_IN_DECK.erase(Cards_IN_DECK.begin()+pickFrom);
	//TO DO: Remove the below line and transfer it to a Discard() function after the player PlayCard() function has been wrote
	Cards_IN_DISCARD.push_back(temp);
	return temp;
}

//Setters and getters
int Deck::size() const { return Cards_IN_DECK.size(); }