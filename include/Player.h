#ifndef PLAYER_H
#define PLAYER_H

#include "Hand.h"
#include "Deck.h"

class Player
{
private:
	int number;
	int Score = 0;
public:
	Player();
	Player(Deck&, int=5, int=1, int=0);
	~Player();
	Hand HAND;
	int numPlayers(int);
	int getScore();
};

#endif