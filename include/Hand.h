#ifndef HAND_H
#define HAND_H

#include <vector>
#include "Deck.h"
#include "Cards.h"

class Hand
{
private:
	int maxCards=0;
	std::vector <Cards> Cards_IN_HAND;
public:
	Hand();
	~Hand();
	void Draw(Deck&);
	void SETmaxCards(int);
	std::string at(int);
	int size();
};

#endif