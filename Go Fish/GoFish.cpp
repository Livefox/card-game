/*
.----------------.  .----------------.   .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. | | .--------------. || .--------------. || .--------------. || .--------------. |
| |    ______    | || |     ____     | | | |  _________   | || |     _____    | || |    _______   | || |  ____  ____  | |
| |  .' ___  |   | || |   .'    `.   | | | | |_   ___  |  | || |    |_   _|   | || |   /  ___  |  | || | |_   ||   _| | |
| | / .'   \_|   | || |  /  .--.  \  | | | |   | |_  \_|  | || |      | |     | || |  |  (__ \_|  | || |   | |__| |   | |
| | | |    ____  | || |  | |    | |  | | | |   |  _|      | || |      | |     | || |   '.___`-.   | || |   |  __  |   | |
| | \ `.___]  _| | || |  \  `--'  /  | | | |  _| |_       | || |     _| |_    | || |  |`\____) |  | || |  _| |  | |_  | |
| |  `._____.'   | || |   `.____.'   | | | | |_____|      | || |    |_____|   | || |  |_______.'  | || | |____||____| | |
| |              | || |              | | | |              | || |              | || |              | || |              | |
| '--------------' || '--------------' | | '--------------' || '--------------' || '--------------' || '--------------' |
'----------------'  '----------------'   '----------------'  '----------------'  '----------------'  '----------------'

*/

#include "GoFish.h"
#include "../include/Deck.h"
#include "../include/Player.h"
#include "../include/Cards.h"

GoFish::GoFish()
{
}
